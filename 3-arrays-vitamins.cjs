const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/

function get_all_available_items(items) {
    let allAvaliableItems = items.filter((currentItem) => {
        if (currentItem.available === true) {
            return currentItem;
        }
    }, {});
    return allAvaliableItems;
}

function get_all_item_containing_only_vitamin_C(items) {
    let itemsWithVitaminsC = items.filter((currentItem) => {
        if (currentItem.contains === "Vitamin C" && currentItem.contains.length == 9) {
            return currentItem;
        }
    }, {});
    return itemsWithVitaminsC;
}

function get_all_item_with_vitamin_A(items) {
    let itemWIthVitaminA = items.filter((currentItem) => {
        if (currentItem.contains.includes("Vitamin A")) {
            return currentItem;
        }
    }, {});
    return itemWIthVitaminA;
}

function group_items(items) {
    let groupedItems = items.reduce((acc, currentItem) => {
        const vitamins = currentItem.contains.split(", ").map(vitamin => {
            if (!acc[vitamin]) {
                acc[vitamin] = [];
            }
            acc[vitamin].push(currentItem.name);
            return vitamin;
        });
        return acc;
    }, {});
    return groupedItems;

}
function custom_Comperator(item1, item2) {
    let Vitamin1 = item1.contains.split(",").length;
    let Vitamin2 = item2.contains.split(",").length;
    if (Vitamin2 > Vitamin1) return 1;
    else if (Vitamin2 - Vitamin1) return -1;
    else return 0;
}

function sort_all_elements(items) {
    let sortedResult = items.sort(custom_Comperator);
    return sortedResult;

}
